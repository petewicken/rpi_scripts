import RPi.GPIO as gpio


class Output(object):

    def __init__(self, port_number, verbose=False, numbering=gpio.BOARD):
        gpio.setmode(numbering)
        self.verbose = verbose

        self._gpio = port_number
        self._power = False

        gpio.setup(self._gpio, gpio.OUT)
        if self.verbose:
            print ('Output {} setup.'.format(self._gpio))

    def set_power(self, state):
        self._power = state
        if self.verbose:
            print ('Power: {}'.format(self._power))
        gpio.output(self._gpio, state)

    def toggle_power(self):
        self.set_power(not self._power)

    def get_state(self):
        return self._power
