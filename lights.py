import time
import signal
import sys
import RPi.GPIO as gpio

from output import Output


def signal_handler(signal, frame):
        print('Cleaning up..')
        gpio.cleanup()
        sys.exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    gpio.setmode(gpio.BOARD)

    output = Output(11, verbose=True)

    while True:
        output.toggle_power()
        time.sleep(1)
